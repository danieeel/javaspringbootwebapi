package com.danielfr.java.capacity.exercises;

import java.io.Console;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyJavaLearningApplication {

	public static void main(String[] args) {

		SpringApplication.run(MyJavaLearningApplication.class, args);


	}

}
